import React from 'react';
import Avatar from '../src';

const AvatarSquare = () => (
  <div style={{ display: 'flex', justifyContent: 'space-between' }}>
    <Avatar name="xxlarge" size="xxlarge" appearance="square" />
    <Avatar name="xlarge" size="xlarge" presence="online" appearance="square" />
    <Avatar name="large" size="large" presence="offline" appearance="square" />
    <Avatar name="medium" size="medium" presence="busy" appearance="square" />
    <Avatar name="small" size="small" presence="focus" appearance="square" />
    <Avatar name="xsmall" size="xsmall" appearance="square" />
  </div>
);

export default AvatarSquare;
