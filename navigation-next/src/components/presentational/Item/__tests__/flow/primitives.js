import React from 'react';

import ItemPrimitive, { ItemPrimitiveBase } from '../../primitives';

const hocProvidedProps = {
  theme: {
    mode: {
      item: () => ({
        container: {},
        product: {},
      }),
    },
    context: 'product',
  },
};

/**
 * ItemPrimitiveBase
 */
<ItemPrimitiveBase {...hocProvidedProps} />;
<ItemPrimitiveBase {...hocProvidedProps} component={() => null} />;

<ItemPrimitiveBase id={5} />;
<ItemPrimitiveBase component={null} />;

/**
 * ItemPrimitive
 */
<ItemPrimitive />;
<ItemPrimitive component={() => null} />;

<ItemPrimitive id={5} />;
<ItemPrimitive component={null} />;
