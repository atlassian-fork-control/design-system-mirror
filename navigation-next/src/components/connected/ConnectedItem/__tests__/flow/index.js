import React from 'react';
import ConnectedItem from '../../index';

<ConnectedItem />;
<ConnectedItem after={null} />;

<ConnectedItem id={5} />;
<ConnectedItem goTo={5} />;
<ConnectedItem after={5} />;
