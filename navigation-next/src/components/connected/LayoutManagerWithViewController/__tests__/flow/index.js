import React from 'react';
import LayoutManagerWithViewController from '../../index';

<LayoutManagerWithViewController globalNavigation={() => null}>
  Page
</LayoutManagerWithViewController>;

<LayoutManagerWithViewController globalNavigation={() => null} />;

<LayoutManagerWithViewController>Page</LayoutManagerWithViewController>;

<LayoutManagerWithViewController
  globalNavigation={() => null}
  itemsRenderer={() => null}
>
  Page
</LayoutManagerWithViewController>;

<LayoutManagerWithViewController
  globalNavigation={() => null}
  containerSkeleton={() => null}
>
  Page
</LayoutManagerWithViewController>;

<LayoutManagerWithViewController
  globalNavigation={() => null}
  navigationViewController={() => null}
>
  Page
</LayoutManagerWithViewController>;
<LayoutManagerWithViewController
  globalNavigation={() => null}
  navigationUIController={() => null}
>
  Page
</LayoutManagerWithViewController>;
