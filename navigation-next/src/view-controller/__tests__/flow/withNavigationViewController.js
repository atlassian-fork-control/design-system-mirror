import React, { Component } from 'react';
import ViewController from '../../ViewController';
import withNavigationViewController from '../../withNavigationViewController';

class Bar extends Component {
  render() {
    return null;
  }
}

<Bar navigationViewController="test" />;

/**
 * BarWithNavigationViewController
 */

const BarWithNavigationViewController = withNavigationViewController(Bar);

<BarWithNavigationViewController />;
