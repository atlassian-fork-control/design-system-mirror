/** @jsx jsx */
import { jsx } from '@emotion/core';

import { sectionCSS } from './styles';
import { SectionProps } from '../types';

const Section = ({
  isScrollable,
  hasSeparator,
  testId,
  ...rest
}: SectionProps) => (
  <div
    // NOTE: Firefox allows elements that have "overflow: auto" to gain focus (as if it had tab-index="0")
    // We have made a deliberate choice to leave this behaviour as is.
    css={sectionCSS(isScrollable, hasSeparator)}
    data-testid={testId}
    // this is being used to target CSS selectors
    // where emotion's API was falling a little short.
    data-section
    {...rest}
  />
);

export default Section;
