/** @jsx jsx */
import { jsx } from '@emotion/core';

import { skeletonHeadingItemCSS } from './styles';
import { SkeletonHeadingItemProps } from '../types';

const SkeletonHeadingItem = ({
  width,
  testId,
  isShimmering,
}: SkeletonHeadingItemProps) => (
  <div css={skeletonHeadingItemCSS(width, isShimmering)} data-testid={testId} />
);

export default SkeletonHeadingItem;
