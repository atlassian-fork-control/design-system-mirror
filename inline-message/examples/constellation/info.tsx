import React from 'react';
import InlineMessage from '../../src';

export default function Info() {
  return (
    <InlineMessage type="info" title="Bitbucket" secondaryText="Learn more">
      <h4>Want more information?</h4>
      <p>
        <a href="#">Log in</a> to setup your account
      </p>
    </InlineMessage>
  );
}
